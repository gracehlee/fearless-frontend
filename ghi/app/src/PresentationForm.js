import React, { useEffect, useState } from 'react';

function PresentationForm () {
        const [conferences, setConferences] = useState([]);

        const [formData, setFormData] = useState({
            title: '',
            presenterName: '',
            presenterEmail: '',
            companyName: '',
            synopsis: '',
            created: '',
            conference: '',
        });

        const handleFormInput = (event) => {
            setFormData({
                ...formData,
                [event.target.name]: event.target.value,
            })
        };

        const handleTitleChange = handleFormInput;
        const handlePresenterNameChange = handleFormInput;
        const handlePresenterEmailChange = handleFormInput;
        const handleCompanyNameChange = handleFormInput;
        const handleSynopsisChange = handleFormInput;
        const handleCreatedChange = handleFormInput;
        const handleConferenceChange = handleFormInput;

        const fetchData = async () => {
            const url = 'http://localhost:8000/api/conferences/';

            const response = await fetch(url);

            if (response.ok) {
                const data = await response.json();
                setConferences(data.conferences);
            }
        };

        useEffect(() => {
            fetchData();
        }, []);

        const handleSubmit = async (event) => {
            event.preventDefault();

            const data = {};
            data.title = formData.title;
            data.presenter_name = formData.presenterName;
            data.presenter_email = formData.presenterEmail;
            data.conference_name = formData.conferenceName;
            data.synopsis = formData.synopsis;
            data.created = formData.created;
            data.conference = formData.conference;
            const conferenceId = data.conference;
            console.log(data);

            const conferenceUrl = `http://localhost:8000/api/conferences/${conferenceId}/presentations/`;
            const fetchConfig = {
                method: "post",
                body: JSON.stringify(data),
                headers: {
                'Content-Type': 'application/json',
                },
            };

            const response = await fetch(conferenceUrl, fetchConfig);
            if (response.ok) {
                const newConference = await response.json();
                console.log(newConference);

                setFormData({
                    title: '',
                    presenterName: '',
                    presenterEmail: '',
                    companyName: '',
                    synopsis: '',
                    created: '',
                    conference: '',
                });
            };
        };

        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new presentation</h1>
                        <form onSubmit={handleSubmit} id="create-presentation-form">
                            <div className="form-floating mb-3">
                                <input onChange={handleTitleChange} value={formData.title} placeholder="Title" required type="text" name="title" id="title" className="form-control"/>
                                <label htmlFor="title">Title</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handlePresenterNameChange} value={formData.presenterName} placeholder="Presenter Name" required type="text" name="presenterName" id="presenter_name" className="form-control"/>
                                <label htmlFor="presenter_name">Presenter Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handlePresenterEmailChange} value={formData.presenter_email} placeholder="Presenter Email" required type="email" name="presenterEmail" id="presenter_email" className="form-control"/>
                                <label htmlFor="presenter_email">Presenter Email</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleCompanyNameChange} value={formData.company_name} placeholder="Company Name" required type="text" name="companyName" id="company_name" className="form-control"/>
                                <label htmlFor="company_name">Company Name</label>
                            </div>
                            <div className="mb-3">
                                <textarea onChange={handleSynopsisChange} value={formData.synopsis} placeholder="Synopsis" name="synopsis" id="synopsis" className="form-control" rows="5"></textarea>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleCreatedChange} value={formData.created} placeholder="Created" required type="date" name="created" id="created" className="form-control"/>
                                <label htmlFor="created">Created</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={handleConferenceChange} value={formData.conference} required id="conference" name="conference" className="form-select">
                                <option value="">Choose a Conference</option>
                                {conferences.map(conference => {
                                    return (
                                        <option value={conference.href.split("/")[3]} key={conference.href}>
                                            {conference.name}
                                        </option>
                                    );
                                })};
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
};

export default PresentationForm;
