import React, { useEffect, useState } from 'react';

function ConferenceForm () {
        const [locations, setLocations] = useState([]);

        const [formData, setFormData] = useState({
            name: '',
            starts: '',
            ends: '',
            description: '',
            maxPresentations: '',
            maxAttendees: '',
            location: '',
        });

        const handleFormInput = (event) => {
            setFormData({
                ...formData,
                [event.target.name]: event.target.value,
            })
        };

        const handleNameChange = handleFormInput;
        const handleStartsChange = handleFormInput;
        const handleEndsChange = handleFormInput;
        const handleDescriptionChange = handleFormInput;
        const handleMaxPresentationsChange = handleFormInput;
        const handleMaxAttendeesChange = handleFormInput;
        const handleLocationChange = handleFormInput;

        const fetchData = async () => {
            const url = 'http://localhost:8000/api/locations/';

            const response = await fetch(url);

            if (response.ok) {
                const data = await response.json();
                setLocations(data.locations);
            }
        };

        useEffect(() => {
            fetchData();
        }, []);

        const handleSubmit = async (event) => {
            event.preventDefault();

            const data = {};
            data.name = formData.name;
            data.starts = formData.starts;
            data.ends = formData.ends;
            data.description = formData.description;
            data.max_presentations = formData.maxPresentations;
            data.max_attendees = formData.maxAttendees;
            data.location = formData.location;
            console.log(data);

            const conferenceUrl = 'http://localhost:8000/api/conferences/';
            const fetchConfig = {
                method: "post",
                body: JSON.stringify(data),
                headers: {
                'Content-Type': 'application/json',
                },
            };

            const response = await fetch(conferenceUrl, fetchConfig);
            if (response.ok) {
                const newConference = await response.json();
                console.log(newConference);

                setFormData({
                    name: '',
                    starts: '',
                    ends: '',
                    description: '',
                    maxPresentations: '',
                    maxAttendees: '',
                    location: '',
                });
            };
        };

        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new conference</h1>
                        <form onSubmit={handleSubmit} id="create-conference-form">
                            <div className="form-floating mb-3">
                                <input onChange={handleNameChange} value={formData.name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleStartsChange} value={formData.starts} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control"/>
                                <label htmlFor="starts">Starts</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleEndsChange} value={formData.ends} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control"/>
                                <label htmlFor="ends">Ends</label>
                            </div>
                            <div className="mb-3">
                                <textarea onChange={handleDescriptionChange} value={formData.description} placeholder="Description" name="description" id="description" className="form-control" rows="5"></textarea>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleMaxPresentationsChange} value={formData.max_presentations} placeholder="Max Presentations" required type="number" name="maxPresentations" id="max_presentations" className="form-control"/>
                                <label htmlFor="max_presentations">Max Presentations</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleMaxAttendeesChange} value={formData.max_attendees} placeholder="Max Attendees" required type="number" name="maxAttendees" id="max_attendees" className="form-control"/>
                                <label htmlFor="max_attendees">Max Attendees</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={handleLocationChange} value={formData.location} required id="location" name="location" className="form-select">
                                    <option value="">Choose a Location</option>
                                    {locations.map(location => {
                                        return (
                                        <option value={location.href.split("/")[3]} key={location.href}>
                                            {location.name}
                                        </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
};

export default ConferenceForm;
