function createCard(name, subtitle, description, pictureUrl, start, end) {
    let card = document.createElement('div');
    card.innerHTML = `
        <div class="card">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${subtitle}</h6>
                <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">
                ${start} - ${end}
            </div>
        </div>
    `;
    return card;
  }

  function createPlaceholder() {
    return `
        <div class="card" aria-hidden="true">
            <img src="https://images.pexels.com/photos/7794362/pexels-photo-7794362.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2" class="card-img-top" alt="">
            <div class="card-body">
                <h5 class="card-title placeholder-glow">
                    <span class="placeholder col-6"></span>
                </h5>
                <p class="card-text placeholder-glow">
                    <span class="placeholder col-7"></span>
                    <span class="placeholder col-4"></span>
                    <span class="placeholder col-4"></span>
                    <span class="placeholder col-6"></span>
                    <span class="placeholder col-8"></span>
                </p>
                <div class="card-footer"></div>
            </div>
        </div>
    `;
  }


function alertMe(error) {
    return `
    <div class="alert alert-danger d-flex align-items-center" role="alert">
    <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
    <div>
      ${error}
    </div>
    </div>
    `;
}


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
    const columns = document.querySelectorAll(".col");
    const bodyTag = document.querySelector("body");

    try {
        const response = await fetch(url);

        if (!response.ok) {
            let errorMessage = `HTTP Error: ${response.status} ${response.statusText}`;
            console.log(errorMessage);
            let html = alertMe(errorMessage);
            bodyTag.innerHTML += html;
        } else {
            const data = await response.json();

            // placeholder cards
            let colNum = 0;
            for (let i = 0; i < data.conferences.length; i++) {
                const column = columns[colNum];
                let placeholder = createPlaceholder();
                column.innerHTML += placeholder;
                colNum++;
                if (colNum > 2) {
                    colNum = 0;
                }
                const cards = document.querySelectorAll(".card");
                for (let card of cards) {
                    card.style.boxShadow = "15px 15px 15px lightgrey";
                    card.style.marginBottom = "20px";
                };
            }
            // end placeholder cards
            colNum = 0;
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const loc = details.conference.location.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const start = details.conference.starts;
                    const end = details.conference.ends;

                    const dateStart = new Date(start);
                    const startDate = dateStart.toLocaleDateString("en-US");
                    const dateEnd = new Date(end);
                    const endDate = dateEnd.toLocaleDateString("en-US");

                    // replace the placeholder cards
                    const column = columns[colNum];
                    let parent = document.querySelector(`.col.num-${colNum}`);
                    let child = parent.querySelector('.card[aria-hidden="true"]');
                    parent.removeChild(child);
                    const html = createCard(title, loc, description, pictureUrl, startDate, endDate);
                    column.insertBefore(html, column.firstChild);
                    colNum++;
                    if (colNum > 2) {
                        colNum = 0;
                    }

                    // CSS
                    const cards = document.querySelectorAll(".card");
                    for (let card of cards) {
                        card.style.boxShadow = "15px 15px 15px lightgrey";
                        card.style.marginBottom = "20px";
                    };
                    // end CSS
                }
            }
        }
    } catch (e) {
        let errorMessage = `Fetch failed: ${e.message}`;
        console.log(errorMessage);
        let html = alertMe(errorMessage);
        bodyTag.innerHTML += html;
    }
});
