window.addEventListener('DOMContentLoaded', async () => {

    const url = "http://localhost:8000/api/conferences/";
    try {
        const response = await fetch(url);

        if (!response.ok) {
            let errorMessage = `HTTP Error: ${response.status} ${response.statusText}`;
            console.log(errorMessage);
        } else {
            const data = await response.json();

            const conferenceTag = document.getElementById("conference");

            for (let conference of data.conferences) {
                // create option element
                const option = document.createElement('option');
                // retrieve the conference ID from the conference href
                const cHref = location.href.split("/");
                const conferenceId = cHref[3];
                // set the option value to the conference ID
                option.value = conferenceId;
                // Set the '.innerHTML' property of the option element to
                // the conference's name
                option.innerHTML = conference.name;
                // Append the option element as a child of the select tag
                conferenceTag.appendChild(option);
            }

            const formTag = document.getElementById('create-presentation-form');
            formTag.addEventListener('submit', async event => {
                event.preventDefault();
                const formData = new FormData(formTag);
                const json = JSON.stringify(Object.fromEntries(formData.entries()));
                console.log(json);

                // need to reference the value of the selected ID
                // which will be the conference number for the presentationUrl
                const index = conferenceTag.selectedIndex;
                const conferenceId = conferenceTag.options[index].value;

                const presentationUrl = `http://localhost:8000/api/conferences/${conferenceId}/presentations/`;
                const fetchConfig = {
                    method: "post",
                    body: json,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };

                const response = await fetch(presentationUrl, fetchConfig);
                if (response.ok) {
                    formTag.reset();
                    const newPresentation = await response.json();
                    console.log(newPresentation);
                }
            });
        }
    } catch (e) {
        let errorMessage = `Fetch failed: ${e.message}`;
        console.log(errorMessage);
    }
});
