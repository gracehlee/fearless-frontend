window.addEventListener('DOMContentLoaded', async () => {

    const url = "http://localhost:8000/api/locations/";
    try {
        const response = await fetch(url);

        if (!response.ok) {
            let errorMessage = `HTTP Error: ${response.status} ${response.statusText}`;
            console.log(errorMessage);
        } else {
            const data = await response.json();
            const locationTag = document.getElementById("location");

            for (let location of data.locations) {
                // create option element
                const option = document.createElement('option');
                    // retrieve location id from the href string
                    const lHref = location.href.split("/");
                    const locationId = lHref[3];
                // set the option value to the location ID
                option.value = locationId;
                // Set the '.innerHTML' property of the option element to
                // the location's name
                option.innerHTML = location.name;
                // Append the option element as a child of the select tag
                locationTag.appendChild(option);
            }

            const formTag = document.getElementById('create-conference-form');
            formTag.addEventListener('submit', async event => {
                event.preventDefault();
                const formData = new FormData(formTag);
                const json = JSON.stringify(Object.fromEntries(formData.entries()));
                console.log(json);


                const conferenceUrl = 'http://localhost:8000/api/conferences/';
                const fetchConfig = {
                    method: "post",
                    body: json,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };

                const response = await fetch(conferenceUrl, fetchConfig);
                if (response.ok) {
                    formTag.reset();
                    const newConference = await response.json();
                    console.log(newConference);
                }
            });
        }
    } catch (e) {
        let errorMessage = `Fetch failed: ${e.message}`;
        console.log(errorMessage);
    }
});
